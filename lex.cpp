/* Kruger, Benjamin     CS4463      Spring 2013     P4
 *
 * This program prompts user for a source file which is then tested to
 * determine whether it is a well-formed XML document. If it passes this
 * test, the user can then choose to supply a list of defined tags. The
 * document will then be validated against that list.
 *
 * 2013-04-19
 *
 * NO HELP! (unless you count version control, it saved my life on this one.)
 *
 * INCLUDED FILES: main.cpp, typedefs.h, good.xml, bad.xml, badnest.xml,
 * invalid.xml, taglist.txt
 */

//#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <stack>
#include <vector>
#include <string>

#include "typedefs.h"

using namespace std;

string DOCTYPE = "<!doctype html>";

bool getToken(Token& tok, ifstream& ifp, vector<string>&);
void sendResults(ostream& dest, Token& tok);
void createTagTable(vector<string>&, vector<string>&, ifstream&);
bool validate(vector<string>&, vector<string>&, string);

int main()
{
    char ch;                        // Character buffer.
	bool valid = true;
	bool wellFormed = true;
    ifstream ifp;					// Input file.
	ifstream definitionList;        // File containing list of tags.
    ofstream ofp;					// Output/log file.
    string if_name;					// Input file name.
    Token currentToken;
    State_Name state = START;
    stack<Token> tokenStack;        // Stack for testing for proper nesting.
    vector<string> tagList;         // Tags found in source document.
    vector<string> tags;            // Normal tags from tag definition file.
    vector<string> emptyTags;       // Empty tags from tag definition file.
    string doctype;                 // String to hold the file's doctype tag

    cout << "Please enter the name of the input file: ";
    cin >> if_name;

    ifp.open(if_name.c_str());

    if (ifp.fail()) {
        cerr << "Unable to open file :" << if_name;
        exit(EXIT_FAILURE);
    }

    ofp.open("log.txt");

    if (ofp.fail()) {
        cerr << "Unable to open log file. " <<
                "Results echoed to standard output only.";
        exit(EXIT_FAILURE);
    }

    // Read the file's doctype tag and compare it to the value of the global
    // const string DOCTYPE. Error and exit if not the same.
    
    getline(ifp, doctype);
    if (doctype.compare(DOCTYPE)) {
        cerr << "Unrecognized DOCTYPE tag." << endl;
        exit(1);
    }
    
    // This loop reads the file, tokenizing the stream. It also checks for
    // proper nesting (Well-formedness), noting any failure.
    
    while (!ifp.eof()) {
        if (!getToken(currentToken, ifp, tagList)) {
            sendResults(cout, currentToken);
            cout << endl << endl;
            cout << "Error in during lexical analysis.";

            exit(EXIT_SUCCESS);
        }

        if (ifp.fail()) {
			break;
		}

		sendResults(cout, currentToken);
		sendResults(ofp, currentToken);

        if (currentToken.type == OPEN_TAG) {
              tokenStack.push(currentToken);
        }

        if (currentToken.type == CLOSE_TAG) {
            if (tokenStack.top().lexeme == currentToken.lexeme) {
                tokenStack.pop();
            } else {
                wellFormed = false;
                cout << endl << "Error: Improperly nested tag at line " <<
                        currentToken.line_number << ", position " <<
                        currentToken.char_pos << "." << endl << endl <<
                        "(Begin tag is of type " <<
                        tokenStack.top().lexeme <<
                        ", but end tag is of type " <<
                        currentToken.lexeme << ".)" << endl;
                break;
            }
        }

    }

    cout << endl << endl;

    // Below is the validation stage, only to be executed if file is well-
    // formed.

    if (wellFormed) {
        cout << "Document is well-formed." << endl << endl;
        cout << "Would you like to supply a list of defined tags (y/n)? ";
        cin >> ch;

        if (ch != 'N' && ch != 'n') {
            cout << endl << endl <<
                    "NOTE: Tag lists should be supplied as a simple" << endl <<
                    "ASCII file, delimited by newlines. To specify" << endl <<
                    "an empty element, append a forward slash to" << endl <<
                    "the name of the tag (e.g. hr/). " << endl <<
                    "Try \"taglist.txt\" for an example." << endl << endl <<
                     "Please enter the file name of the tag list: ";

            cin >> if_name;

            definitionList.open(if_name.c_str());

            if (definitionList.fail()) {
                cerr << "Unable to open file.";
                exit(EXIT_FAILURE);
            }

            createTagTable(tags, emptyTags, definitionList);

            vector<string>::iterator it;

            cout << "Defined tags (matched): ";

            for (it = tags.begin(); it != tags.end(); it++) {
                cout << *it << " ";
            }

            cout << endl;
            cout << "Defined tags (empty): ";

            for (it = emptyTags.begin(); it != emptyTags.end(); it++) {
                cout << *it << " ";
            }

            cout << endl << endl;


//            vector<string>::iterator it;
            for (it = tagList.begin(); it != tagList.end(); it++) {
                cout << *it << ": ";
                if (!validate(tags, emptyTags, *it)) {
                    cout << "UNDEFINED!" << endl;
                    valid = false;
                    break;
                }
                cout << "defined." << endl;

            }



            if (valid) {
                cout << endl << "Document is valid as per supplied DTD.";
                cout << endl;
            } else {
                cout << endl << "Document is invalid." << endl;
            }
        }
    }
    return EXIT_SUCCESS;
}


/** validate
 *
 * This function searches the two supplied vectors for the supplied string.
 * If it is found, it returns true. Otherwise, it returns false.
 *
 * @param tags vector<string>& - a reference to the list of normal tags.
 * @param emptyTags vector<string>& - a reference to the list of empty tags.
 * @param s string - the search key.
 *
 * @return true, if string is found.
 * @return false, if string is not found.
 */

bool validate(vector<string>& tags, vector<string>& emptyTags, string s) {
    vector<string>::iterator it;
    for (it = tags.begin(); it != tags.end(); it++) {
        if (*it == s) {
            return true;
        }
    }
    for (it = emptyTags.begin(); it != emptyTags.end(); it++) {
        if (*it == s) {
            return true;
        }
    }

    return false;
}


/** createTagTable
 *
 * This function reads a list of tags from a file and shunts each tag into
 * either the list of normal tags or the list of empty tags.
 *
 * @param tags vector<string>& a reference to the list of normal tags.
 * @param emptyTags vector<string>& a reference to the list of emypty tags.
 * @param definitionList ifstream& a reference to the file stream comprising
 *        the list of defined tags.
 *
 * @return void (all changes are to the referenced vectors).
 */

void createTagTable(vector<string>& tags, vector<string>& emptyTags,
					ifstream& definitionList)
{
	bool empty = false;
	string buf;
	while (!definitionList.eof()) {

		getline(definitionList, buf);
        empty = false;
        if (buf[buf.size()-1] == '/')
        {
            empty = true;
        }

		if (empty) {
			buf.erase(buf.end()-1);
			emptyTags.push_back(buf);
		} else {
			tags.push_back(buf);
		}
		if (definitionList.eof()) {
			break;
		}
	}

}


/** getToken
 *
 * This function largely constitutes the lexical analysis stage of operation.
 * It tokenizes the source file.
 *
 * @param tok Token& - a reference to the token currently being identified.
 * @param ifp ifstream& - a reference to the filestream containing the source.
 * @param tagList vector<string>& - a reference to the vector which will
 *        contain the names of the tag tokens.
 *
 * @return void (All changes made are via references.)
 */

bool getToken(Token& tok, ifstream& ifp, vector<string>& tagList)
{
    tok.lexeme = "";
    tok.char_pos = 0;
    tok.type = ERR_TOKEN;
    tok.line_number = 0;
    char c;
    State_Name state = START;
    State_Name prevState = START;
    Char_Type charType;
    static long lineNumber = 1;
    static int charPos = 0;

    while(!ifp.eof()) {
        c = ifp.get();
        if (ifp.fail()) {
            return true;
        }

        charPos++;

        if (state == START) {
            tok.char_pos = charPos;
            tok.line_number = lineNumber;
        }

        charType = char_classes[c];
        state = state_trans[state][charType];

        switch (state) {
            case OT1:
                break;
            case OT2:
                tok.lexeme += c;
                tok.type = OPEN_TAG;
                break;
            case OT3:
                break;
            case CT1:
                tok.type = CLOSE_TAG;
                break;
            case CT2:
                tok.lexeme += c;
                break;
            case CT3:
                tok.lexeme += c;
                return true;
            case ET1:
            	break;
            case SP1:
                tok.type = SPACE_STR;
                break;
            case ETF:
                tok.type = EMPTY_TAG;
                tagList.push_back(tok.lexeme);
                return true;
            case ERF: /* TAG ERROR */
            	tok.type = ERR_TOKEN;
            	ifp.unget();
            	return false;
            case NLF: /* NEWLINE */
                tok.lexeme = "\\n";
                tok.type = NL_TOKEN;
                lineNumber++;
                charPos = 0;
                return true;
            case SPF: /* SPACE STRING */
                tok.lexeme = " ";
                ifp.unget();
                return true;
            case OTF: /* BEGIN_TAG */
                if (tok.lexeme.length() > 7) {
                    tok.lexeme = tok.lexeme.substr(0,6) + '~';
                }
                tagList.push_back(tok.lexeme);
                return true;
            case CTF: /* END_TAG */

                tagList.push_back(tok.lexeme);
                return true;
            case W1:
                tok.lexeme += c;
                break;
            case WF:
                if (tok.lexeme.length() > 7) {
                    tok.lexeme = tok.lexeme.substr(0,6) + '~';
                }
                tok.type = WORD_TOKEN;
                ifp.unget();
                return true;
        }
    }
}


/** sendResults
 *
 * Note: Modified for machine-readability.
 * 
 * This function abstracts the debug log, so that it may be sent to the display
 * as well as the logfile.
 *
 * @param dest ostream& - a reference to the filestream of the logfile.
 * @param currentToken Token& - a reference to the token currently being read.
 *
 * @return void (This procedure's only side effects are screen/file output.)
 */

void sendResults(ostream& dest, Token& currentToken)
{
	dest << left;
	dest << currentToken.type;
    if (currentToken.type != 4 && currentToken.type != 5) {
        dest << currentToken.lexeme;
    }
    dest << endl;
}
