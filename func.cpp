#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#define debug(a) cerr << a;

// GLOBAL:
std::ifstream in;
std::string token;
using namespace std;

string get_token();
void start();
void html_beg();
void head_beg();
void title_beg();
void title_bdy();
void title_end();
void head_end();
void body_beg();
void para_beg();
void para_bdy();
void list_beg();
void li_beg();
void li_bdy();
void link_beg();
void link_bdy();
void body_end();
void html_end();
void error();

int main() {
	
	in.open("T:/users/bekro_000/documents/projects/P5/log.txt");
	
	// verify file is open
	if (!in) {
		cerr << "Unable to open log.txt" << endl;
		cin.ignore();
		exit(1);
	}

	start();

	return EXIT_SUCCESS;
}

void start() {
	// debugging statements
	debug("start.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("0html")) {
		html_beg();
		return;
	} else {
		error();
	}
}

void html_beg() {
	// debugging statements
	debug("html_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("0head")) {
		head_beg();
		return;
	} else {
		error();
	}
}

void head_beg() {
	// debugging statements
	debug("head_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("0title")) {
		title_beg();
		return;
	} else {
		error();
	}
}

void title_beg() {
	// $n statements
	debug("title_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	if (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		title_bdy();
	} else if (!token.compare("1title")) {
		title_end();
	} else {
		error();
	}
}

void title_bdy() {
	// debugging statements
	debug("title_bdy.\ntoken=");

	token = get_token();
	cout << token << endl;

	if (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		title_bdy();
	} else if (!token.compare("1title")) {
		title_end();
	} else {
		error();
	}	
}

void title_end() {
	// debugging statements
	debug("title_end.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("1head")) {
		head_end();
	} else {
		error();
	}
}

void head_end(){
	// debugging statements
	debug("head_end.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("0body")) {
		body_beg();
	} else {
		error();
	}
}

void body_beg() {
	// debugging statements
	debug("body_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	// while the token remains a legitimate child for the body node
	while (token.compare("0p") == 0 || token.compare("0ul") == 0 || token.compare("0a") == 0 || token[0] == '4' || token[0] == '5') {

	//while (!token.compare("0p") && !token.compare("0ul") && !token.compare("0a") && token[0] != '4' && token[0] != '5') {
		debug("\nin the big while loop.\ntoken = ");
		debug(token);
		debug(endl);
		// If it's a paragraph, go to that function. Upon return, get the next token.
		// then begin the next iteration of the loop.
		if (!token.compare("0p")) {
			para_beg();
			token = get_token();
			continue;
		// If it's an unordered list, go to that function. Upon return, get the next token.
		// then begin the next iteration of the loop.
		} else if (!token.compare("0ul")) {
			list_beg();
			token = get_token();
			continue;
		// If it's a hyperlink, go to that function. Upon return, get the next token.
		// then begin the next iteration of the loop.
		} else if (!token.compare("0a")) {
			link_beg();
			token = get_token();
			continue;
		} else if (token[0] == '4' || token[0] == '5') {
			token = get_token();
			continue;
		}

	}


	if (!token.compare("1body")) {
		body_end();
	} else {
		debug("Last token was ");
		debug(token << endl);
		error();
	}
}

void para_beg() {
	// debugging statements
	debug("para_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	if (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		para_bdy();
		debug("\nreturning from para_beg. current token is ");
		debug(token);
		return;
	} else if (!token.compare("1p")) {
		return;
	} else {
		error();
	}
}
void para_bdy() {
	// debugging statements
	debug("para_bdy.\ntoken=");

	token = get_token();
	cout << token << endl;

	if (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		para_bdy();
	} else if (!token.compare("1p")) {
		debug("returning from para_bdy. current token is ");
		debug(token);
		return;
	} else {
		error();
	}	
}
void list_beg() {

	// debugging statements
	debug("list_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token.compare("0li") == 0 || token.compare("0ul") == 0 || token[0] == '4' || token[0] == '5') {
		if (token[0] =='4' || token[0] == '5') {
			token = get_token();
			continue;
		} else if (token.compare("0li") == 0) {
			li_beg();
			token = get_token();
			continue;
		} else if (token.compare("0ul") == 0) {
			list_beg();
			token = get_token();
			continue;
		}
	}
	

	if (token.compare("1ul") == 0) {
		debug("returning from list_beg. token = ");
		debug(token << endl);
		return;
	} else {
		error();
	}
}
void li_beg() {
	// debugging statements
	debug("li_beg.\ntoken=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (token[0] == '3') {
		li_bdy();
		debug("returning from li_beg, token = ");
		debug(token << endl);

		return;
	} else if (token.compare("1li") == 0) {
		return;
	}
}
void li_bdy() {
	// debugging statements
	debug("li_bdy.\ntoken=");

	token = get_token();
	cout << token << endl;

	if (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		li_bdy();
		return;
	} else if (!token.compare("1li")) {
		debug("returning from li_bdy. current token is ");
		debug(token);
		return;
	} else {
		error();
	}	
}
void link_beg() {
	debug("link_beg" << endl);
	debug("token = " << token << endl);
	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		token = get_token();
		cout << token << endl;
	}

	if (token[0] == '3') {
		link_bdy();
		return;
	} else {
		error();
	}
}
void link_bdy() {
	debug("link_bdy.\ntoken = ");
	token = get_token();
	debug(token << endl);
	while (token[0] == '3' || token[0] == '4' || token[0] == '5') {
		link_bdy();
		return;
	}

	if (token.compare("1a") == 0) {
		debug("leaving the link.\n");
		return;
	} else {
		error();
	}
}
void body_end() { 
	// debugging statements
	debug("body_end. token=");

	token = get_token();
	cout << token << endl;

	while (token[0] == '4' || token[0] == '5') {
		debug("token=");
		token = get_token();
		cout << token << endl;
	}

	if (!token.compare("1html")) {
		html_end();
	} else {
		error();
	}
}
void html_end() {
	cout << "html_end\n\n" << "Source file is valid HTMLb" << endl;
	exit(EXIT_SUCCESS);
}
void error() {
	cerr << "Invalid token." << endl;
	exit(2);
}
string get_token() {
	string linebuf;
	if (!in.eof() && !in.fail()) {
		getline(in, linebuf);
	}
	return linebuf;
}