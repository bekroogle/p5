#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

enum State {START, HTML_BEG, HEAD_BEG, TITLE_BEG, TITLE_BDY, TITLE_END, HEAD_END, BODY_BEG, PARA_BEG, PARA_BDY,
	UL_BEG, LI_BEG, LI_END, UL_END, LINK_BEG, LINK_END, BODY_END, HTML_END, ERR};

string state_names[] = {"START", "HTML_BEG", "HEAD_BEG", "TITLE_BEG", "TITLE_BDY", "TITLE_END", "HEAD_END", "BODY_BEG", "PARA_BEG", "PARA_BDY",
						"UL_BEG", "LI_BEG", "LI_END", "UL_END", "LINK_BEG", "LINK_END", "BODY_END", "HTML_END", "ERR"};

void transition(State&, string);
bool is4or5(string);

int main() {
	ifstream infile;
	string linebuf, token;
	State state = START;

	infile.open("log.txt");
	if(!infile) {
		exit(1);
	}

	while (true) {
		getline(infile, linebuf);
		if (infile.eof()) {
			break;
		}
		token = linebuf;
		cout << "State: " << state_names[state] << ", Token: " << token << endl;
		transition(state, token);
	}
	return 0;
}

void transition(State& state, string token) {
	switch(state) {

	// Start state
	case START:
		if (is4or5(token)) {
			state = START;
			return;
		} else if (!token.compare("0html")) {
			state = HTML_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// Html begin state
	case HTML_BEG:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("0head")) {
			state = HEAD_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// Head begin state
	case HEAD_BEG:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("0title")) {
			state = TITLE_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// Title begin state
	case TITLE_BEG:
		if (token[0] >= '3' && token[0] <= '5') {		// token is newline, space or word
			state = TITLE_BDY;
			return;
		} else {
			state = ERR;
			return;
		}
	
	// Title body state
	case TITLE_BDY:

		if (token[0] >= '3' && token[0] <= '5') {		// token is newline, space or word
			return;
		} else if (!token.compare("1title")) {
			state = TITLE_END;
			return;
		} else {
			state = ERR;
			return;
		}
	// Title end state
	case TITLE_END:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("1head")) {
			state = HEAD_END;
			return;
		} else {
			state = ERR;
			return;
		}

	// Head end state
	case HEAD_END:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("0body")) {
			state = BODY_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// Body begin state
	case BODY_BEG:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("1body")) {
			state = BODY_END;
			return;
		} else if (!token.compare("0p")) {
			state = PARA_BEG;
			return;
		} else if (!token.compare("0ul")) {
			state = UL_BEG;
			return;
		} else if (!token.compare("0a")) {
			state = LINK_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// paragraph begin state
	case PARA_BEG:
		if (token[0] > '2' && token[0] < '6') {		// token is newline, space or word
			state = PARA_BDY;
			return;
		} else if (!token.compare("1p")) {
			state = BODY_BEG;
			return;
		} else {
			state = ERR;
			return;
		}

	// paragraph body state
	case PARA_BODY:
		if (token[0] > '2' && token[0] < '6') {		// token is newline, space or word
			state = PARA_BDY;
			return;
		} else if (!token.compare("1p")) {
			state = BODY_BEG;
			return;
		} else {
			state = ERR;
			return;
		}
	case UL_BEG:
		if (!token.compare("0li")) {
			state = LI_BEG;
			return;
		} else if (!token.compare("0ul")) {
			state = UL_BEG;
			return;
		} else if (!token.compare("1ul")) {
			state = 

	// body end state
	case BODY_END:
		if (is4or5(token)) {
			return;
		} else if (!token.compare("1html")) {
			state = HTML_END;
			return;
		} else {
			state = ERR;
			return;
		}

	// error state
	case ERR:
		cerr << "Error, invalid token.";
		exit(2);
	default:
		return;
	}

}


bool is4or5(string source) {
	if (source[0] == '4' || source[0] == '5') {
		return true;
	} else {
		return false;
	}
}